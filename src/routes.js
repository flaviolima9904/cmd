import React from 'react';
import Login from "./login";
import Register from './register';
import MyMap from './map';
import Dash from './dash';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack'


const Routes = createStackNavigator(
    {
        LoginScreen: Login,
        RegisterScreen: Register,
        MapScreen: MyMap,
        DashScreen:Dash
    },
    {
        initialRouteName: 'LoginScreen'
    }
)

export default createAppContainer(Routes);