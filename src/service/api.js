import axios from 'axios';
import { AsyncStorage } from "react-native";
const api = axios.create({
    baseURL:'http://192.168.0.100:8000'
});
api.interceptors.request.use(async config => {
    const token = await AsyncStorage.getItem('token');
    if (token) {
        config.headers.Authorization = `Barrer ${token}`
    }

    return config;
});

export default api;