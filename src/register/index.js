import React, { Component } from 'react';
import { Container, Form, Item, Label, Input, Grid, Row, Button, Text ,Content} from "native-base";
import {ToastAndroid} from 'react-native';
import RegisterStyles from './style';
import api from '../service/api';


class Register extends Component {
    state = {
        name:'',
        email:'',
        password:'',
        isLoading:false,
    }
    static navigationOptions = {
        title: 'Registrar',
        headerStyle: {
          backgroundColor: '#8e44ad',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      };
      handleRegister = async () => {
        const params = this.state;
        this.setState({isLoading:true})
        const { data } = await api.post('/api/auth/register',params);
        const { navigation } = this.props;
        if (data.ok) {
            this.setState({isLoading:false})
            ToastAndroid.show(data.msg, ToastAndroid.SHORT,ToastAndroid.BOTTOM);
            navigation.navigate('LoginScreen');
        }
      }

    render() {
        const { navigation } = this.props;
        return (
            <Container style={RegisterStyles.conteiner}>
               <Content>
                <Form>
                <Item floatingLabel>
                    <Label>Nome</Label>
                    <Input onChangeText={(value) => this.setState({name:value})}/>
                    </Item>
                    <Item floatingLabel>
                    <Label>Email</Label>
                    <Input onChangeText={(value) => this.setState({email:value})} />
                    </Item>
                    <Item floatingLabel>
                    <Label>Senha</Label>
                    <Input secureTextEntry={true} onChangeText={(value) => this.setState({password:value})}/>
                    </Item>
                    <Grid style={RegisterStyles.gridButtons}>
                        <Row>
                            <Button
                                disabled={this.state.isLoading}
                             style={RegisterStyles.buttons}
                             onPress={this.handleRegister}
                             >
                            <Text>{this.state.isLoading ? 'Processando...' : 'Registrar'}</Text>
                            </Button>
                        </Row>
                    </Grid>
                </Form>
                </Content>
            </Container>
        )
    }
}

export default Register;