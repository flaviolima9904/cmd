import {StyleSheet } from "react-native";

const styles = StyleSheet.create({
    conteiner:{
        marginTop: 10
    },
    gridButtons:{
        justifyContent:'center',
        margin:20
    },
    buttons:{
        width:'100%',
        justifyContent:'center',
        backgroundColor:'#8e44ad'
    }
});

export default styles;