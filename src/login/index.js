import React, { Component } from 'react';
import { Container,Label, Content, Form, Item, Input, Button,Text, Grid, Col } from 'native-base';
import LoginStyles from "./style";
import api from '../service/api';
import { AsyncStorage,ToastAndroid } from "react-native";
export default class FormExample extends Component {
  static navigationOptions = {
    title: 'Realizar Login',
    headerStyle: {
      backgroundColor: '#8e44ad',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };
  state = {
    email:'',
    password:'',
    isLoading:false
  }
   handleLogin = async() => {

    const params = this.state;
    const { data } = await api.post('/api/auth/login',params);
    const {navigation} = this.props;
    this.setState({isLoading:true});
    if (data.ok) {
      this.setState({isLoading:false});
     await AsyncStorage.setItem('authUser',JSON.stringify(data.user))
      navigation.push('DashScreen');
    }else{
      ToastAndroid.show(data.msg, ToastAndroid.SHORT,ToastAndroid.BOTTOM);
      this.setState({isLoading:false});
    }
  }
  render() {
    const { navigation } = this.props;
    return (
      <Container style={LoginStyles.conteiner}>
        <Content>
          <Form >
            <Item floatingLabel>
            <Label>Email</Label>
              <Input placeholder="Email" onChangeText={(value) => this.setState({email:value})}/>
            </Item>
            <Item floatingLabel>
            <Label>Senha</Label>
            <Input placeholder="Senha" secureTextEntry={true} onChangeText={(value) => this.setState({password:value})}/>
            </Item>
            <Grid style={LoginStyles.gridButtons}>
            <Col style={LoginStyles.col}>
              <Button 
              disabled={this.state.isLoading}
              style={LoginStyles.buttons}
              onPress={this.handleLogin}
              >
              <Text>{this.state.isLoading ? 'Processando...' : 'Entrar'}</Text>
              </Button>
              </Col>
            <Col style={{margin:10}}>
            <Button 
             disabled={this.state.isLoading}
             style={LoginStyles.buttons}
             onPress={() => navigation.push('RegisterScreen')}
              >
              <Text>Registrar-se</Text>
              </Button>
              </Col>
            </Grid>
          </Form>
        </Content>
      </Container>
    );
  }
}
