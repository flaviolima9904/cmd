import React, { Component } from 'react';
import { Container, Label, Content, Form, Item, Input, Button, Text, Grid, Col } from 'native-base';

import { AsyncStorage } from "react-native";
export default class FormExample extends Component {
    static navigationOptions = {
        title: 'Realizar Login',
        headerStyle: {
            backgroundColor: '#8e44ad',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };
    state = {
        authUser: {}
    }
    componentDidMount = async () => {
        const user = await AsyncStorage.getItem('authUser');
        this.setState({ authUser: JSON.parse(user) });
    }
    render() {

        return (
            <Container style={{ margin: 10, justifyContent: 'center' }}>
                <Content>
                    <Text>Bem Vindo: {this.state.authUser.name}</Text>
                    <Text>Seu email é: {this.state.authUser.email}</Text>
                </Content>
            </Container>
        );
    }
}
